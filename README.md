# Grupo 01

Repositorio para el curso Análisis de Big Data

## Integrantes

| Nombre                        | email                      | Usuario de GitHub    |
| ----------------------------- | -------------------------- | -------------------- |
| Julian Andres Misnaza Morales | ja.misnaza@uniandes.edu.co | julianmisnazamorales |
| Sergio David Romero Maldonado | sd.romero@uniandes.edu.co  | sromerouniandes      |
| Oscar Javier Angel Balcázar   | o.angel@uniandes.edu.co    | ojangelb             |

## Ambiente de desarrollo

Para cconfigurar el ambiente de desarrollo se deben de contar con las siguientes herramientas:

- IDE Εclipse
- Java 8 
- Hadoop 3.1.1 
- Cluster Hadoop 
-  Dataset de pruebas 

## Proyectos
Las tareas se ejecutan de manera independiente, llamando a cada uno de los jobs bajo la siguiente estrutura:
- Comando para ejecutar job en Hadoop. Puede ser hadoop jar o yarn jar
- Compilado de la aplicación 
- Ruta del paquete donde se encuentra la tarea a ejecutar
- Ruta donde se encuentran los datos que seran procesados
- Ruta del fichero donde seran registradas las salidas tras ejecutar la tarea.  
Nota: Debido al alto consumo de memoria por parte de las tareas es necesario agregar una variable de entorno de Hadoop en cada ejecución (HADOOP_CLIENT_OPTS="-Xmx4g"), con la cual se asigna mas espacio de memoria para lograr la ejecución satisfactoriamente  
### Tarea 1 - Map Reduce
Bajo la anterior premisa, las ejecuciones de las respectivas tareas deben realizarse de la siguiente manera:
- HADOOP_CLIENT_OPTS="-Xmx4g" hadoop jar WordCountMR-0.0.1-SNAPSHOT.jar uniandes.reuters.job.WordCounterA /datos/reuters/individual  outputX
- HADOOP_CLIENT_OPTS="-Xmx4g" hadoop jar WordCountMR-0.0.1-SNAPSHOT.jar uniandes.reuters.job.WordCounterB /datos/reuters/individual  outputX
- HADOOP_CLIENT_OPTS="-Xmx4g" hadoop jar WordCountMR-0.0.1-SNAPSHOT.jar uniandes.reuters.job.WordCounterC1 /datos/reuters/individual  outputX
- HADOOP_CLIENT_OPTS="-Xmx4g" hadoop jar WordCountMR-0.0.1-SNAPSHOT.jar uniandes.reuters.job.WordCounterC2 /datos/reuters/individual  outputX
- HADOOP_CLIENT_OPTS="-Xmx4g" hadoop jar WordCountMR-0.0.1-SNAPSHOT.jar uniandes.reuters.job.WordCounterC3 /datos/reuters/individual  outputX
- HADOOP_CLIENT_OPTS="-Xmx4g" hadoop jar WordCountMR-0.0.1-SNAPSHOT.jar uniandes.reuters.job.WordCounterD /datos/reuters/individual  outputX
- HADOOP_CLIENT_OPTS="-Xmx4g" hadoop jar WordCountMR-0.0.1-SNAPSHOT.jar uniandes.webhose.job.WordCounterE data outputX
- HADOOP_CLIENT_OPTS="-Xmx4g" hadoop jar WordCountMR-0.0.1-SNAPSHOT.jar uniandes.webhose.job.WordCounterF data outputX                                                         

Nota: La X despues de la palabra "output" hace referencia a una secuencia aleatoria no repetida que debe colocarse en seguida de cada uno de los nombres de las carpetas de salida.

Es importante tener en cuenta que para la ejecución de los procesos de los numerales E y F se deben de pasar los archivos a la carpeta data, el cual es el resultado del procesamiento previo de los archivos individuales de noticias. 

### Resultados Tarea 1
Los resultados son registrados dentro del HDFS del cluster de Hadoop instalado en la maquina, para acceder a los resultados obtenidos, puede ejecutar el comando: 
- hadoop fs -get nombrearchivo rutadestino

### Tarea 2 - Spark

Para realizar la ejecución en el cluster se deben de seguir los siguientes pasos:

1. Copiar al cluster el script de Python a la carpeta home.

2. Descargar en la carpeta home el script correspondiente para el prosesamiento de las zonas (GeoJson) en formato .json

3. Dentro del clúster previa instalación de conda se debe creae un viatual env 

   ​	`conda create -y -n tarea2grupo01v10 -c wheel pandas conda-pack geopandas shapely python=2.7`

4. Posteriormente se activa el virtual env y se exportan las siguientes variables:

      `conda activate tarea2grupo01v10`

      `export PYSPARK_DRIVER_PYTHON=python`

      `export PYSPARK_PYTHON=./environment/bin/python`

5. Después se debe empaquetar el ambiente y replicarlo en el sistema de archivos del cluster:

   `conda pack -f -o tarea2grupo01v10.tar.gz`

   `pyspark --archives tarea2grupo01v10.tar.gz#environment` 

6. Para finalizar se debe ejecutar el comando spark-submit para iniciar el procesamiento del job en el custer:

   `spark-submit --deploy-mode cluster --master yarn         --driver-memory 2g --executor-memory 2g         --num-executors 4 --executor-cores 2         --conf spark.dynamicAllocation.enabled=false         ç--archives tarea2grupo01v10.tar.gz#environment`

Para ejecutar  el Notebook se deben de seguir las siguientes instrucciones:

1.  Tener previeamente instalado el software Jupiter-Notebook e importar el arvicho *Tarea2_Spark_Files.ipynb* 

2. Descargar los archivos de la carpeta *Archivos_Entrada_Notebook*

3. Descargaar el archivo de localizacion de Nueva York en formato .json 

4. En las celdas con pocisión cuarta, quinta y sexta, se deberán actualizar los *path* en los cuales se descargaron los archivos de los anteriores pasos y adicionalmente se debe modificar el *path* de los archivos de salida.

5. Ejecutar el notebook, deacuerdo a la configuración del *path* se deben de verificar los archivos de salida en formato cdv.

    

### Resultados Tarea 2

Los resultados son registrados dentro de este repositorio en la siguiente carpeta:

- Archivos_Salida_Notebook



### Taller 1 - Spark MongoDB

Para realizar la ejecución en el cluster se deben de seguir los siguientes pasos:

1. Copiar al cluster el script de Python a la carpeta home.

2. Descargar en la carpeta home el archivo  correspondiente para la geolocalización de los estados.

3. Dentro del clúster previa instalación de conda se debe creae un viatual env 

   ​	`conda create -y -n tarea2grupo01v10 -c wheel pandas conda-pack geopandas shapely python=2.7`

4. Posteriormente se activa el virtual env y se exportan las siguientes variables:

   `conda activate tarea2grupo01v10`

   `export PYSPARK_DRIVER_PYTHON=python`

   `export PYSPARK_PYTHON=./environment/bin/python`

5. Después se debe empaquetar el ambiente y replicarlo en el sistema de archivos del cluster:

   `conda pack -f -o tarea2grupo01v10.tar.gz`

   `pyspark --archives tarea2grupo01v10.tar.gz#environment` 

6. Para finalizar se debe ejecutar el comando spark-submit para iniciar el procesamiento del job en el custer:

   Cluster No. 1:

   `spark-submit --driver-memory 5g --driver-cores 6 --executor-memory 5g   --num-executors 18 --executor-cores 1  --archives taller1grupo01v2.tar.gz#environment --packages org.mongodb.spark:mongo-spark-connector_2.11:2.3.5        taller_1.py &`

   Cluster No. 2:

   `spark-submit --driver-memory 5g --driver-cores 6 --executor-memory 5g   --num-executors 18 --executor-cores 1  --archives taller1grupo01v2.tar.gz#environment --packages org.mongodb.spark:mongo-spark-connector_2.11:2.3.5        taller_2.py &`



### Resultados Taller 1

El resultado del procesamiento de datos se encuentra en el cluster de Mongo `bigdata-mongodb-01.virtual.uniandes.edu.co`y pueden ser consultados por medio de la aplicacion de resultado: `http://172.24.99.146/`  se requiere que el navegador tenga configurado el proxy de la universidad `connect.virtual.uniandes.edu.co:443`


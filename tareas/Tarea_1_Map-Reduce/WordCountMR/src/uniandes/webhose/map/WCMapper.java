package uniandes.webhose.map;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import uniandes.stopwords.StopWordsService;

import java.io.IOException;
import java.util.HashMap;

public class WCMapper extends Mapper<LongWritable, Text, Text, IntWritable> {

	public static final String JSON_KEY = "key";
	
	@Override
	protected void map(LongWritable key, Text value,
			Context context)
			throws IOException, InterruptedException {

		Configuration conf = context.getConfiguration();
		String json_key = conf.get(JSON_KEY);

		String json = value.toString();
		ObjectMapper objectMapper = new ObjectMapper();
		JsonNode jsonNode = objectMapper.readTree(json);

		if (jsonNode.hasNonNull(json_key)) {

			String palabrasJsonNode = jsonNode.get(json_key).asText();

			HashMap<String, Integer> palabrasLinea = new HashMap<String, Integer>();
			String[] palabras = palabrasJsonNode.toString().split("([().,!?:;'\"-></]|\\s)+");
			for (String palabra : palabras) {
				String lw = palabra.toLowerCase().trim();
				if (lw.equals("")) {
					continue;
				} //No queremos contar espacios
				//Si la palabra existe en el hashmap incrementa en 1 su valor,
				//en caso contrario la agrega y le asigna 1.

				if (!StopWordsService.isSpanishStopWord(lw)) {
					palabrasLinea.put(lw,
							palabrasLinea.containsKey(lw) ?
									(palabrasLinea.get(lw) + 1)
									: 1);
				}
			}
			for (String k : palabrasLinea.keySet()) {
				context.write(new Text(k), new IntWritable(palabrasLinea.get(k)));
			}
		}
		
	}
}

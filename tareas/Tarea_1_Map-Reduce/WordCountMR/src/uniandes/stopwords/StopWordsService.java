package uniandes.stopwords;

import org.apache.commons.lang3.ArrayUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class StopWordsService {

    public static boolean isSpanishStopWord (String word) {

        String content;
        String[] stopsWords = {""};
        try {
            content = new String(Files.readAllBytes(Paths.get("resources/spanish.txt")));
            stopsWords = content.split("([().,!?:;'\"-></]|\\s)+");

        } catch (IOException e) {
            e.printStackTrace();
        }
        return ArrayUtils.contains(stopsWords,word);
    }

    public static boolean isEnglishStopWord (String word) {

        String content;
        String[] stopsWords = {""};
        try {
            content = new String(Files.readAllBytes(Paths.get("resources/english.txt")));
            stopsWords = content.split("([().,!?:;\"-></]|\\s)+");

        } catch (IOException e) {
            e.printStackTrace();
        }
        return ArrayUtils.contains(stopsWords,word);
    }
}

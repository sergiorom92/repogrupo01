package uniandes.reuters.job;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import uniandes.reuters.mapRed.WCMapperC3;
import uniandes.reuters.mapRed.WCReducerC3;
import uniandes.reuters.util.XmlInputFormat;

import java.io.IOException;

public class WordCounterC3 {
	public static void main(String[] args)  {
		if(args.length<2){
			System.out.println("Se necesitan las carpetas de entrada y salida");
			System.exit(-1);
		}

		String entrada = args[0]; //carpeta de entrada
		String salida = args[1];//La carpeta de salida no puede existir

		try {
			ejecutarJob(entrada, salida);
		} catch (Exception e) { //Puede ser IOException, ClassNotFoundException o InterruptedException
			e.printStackTrace();
		}

	}
	public static void ejecutarJob(String entrada, String salida) throws IOException,ClassNotFoundException, InterruptedException
	{
		/**
		 * Objeto de configuraci�n, dependiendo de la versi�n de Hadoop
		 * uno u otro es requerido.
		 * */
		Configuration conf = new Configuration();
		conf.set("xmlinput.start", "<PLACES>");
		conf.set("xmlinput.end", "</BODY>");
		Job wcJob=Job.getInstance(conf, "WordCounter Job C3");
		wcJob.setJarByClass(WordCounterC3.class);
		//////////////////////
		//Mapper
		//////////////////////

		wcJob.setMapperClass(WCMapperC3.class);

		wcJob.setMapOutputKeyClass(Text.class);
		wcJob.setMapOutputValueClass(IntWritable.class);
		///////////////////////////
		//Reducer
		///////////////////////////
		wcJob.setReducerClass(WCReducerC3.class);
		wcJob.setOutputKeyClass(Text.class);
		wcJob.setOutputValueClass(IntWritable.class);

		///////////////////////////
		//Input Format
		///////////////////////////
		//Advertencia: Hay dos clases con el mismo nombre,
		//pero no son equivalentes.
		//Se usa, en este caso, org.apache.hadoop.mapreduce.lib.input.TextInputFormat
		XmlInputFormat.setInputPaths(wcJob, new Path(entrada));
		wcJob.setInputFormatClass(XmlInputFormat.class);

		////////////////////
		///Output Format
		//////////////////////
		TextOutputFormat.setOutputPath(wcJob, new Path(salida));
		wcJob.setOutputFormatClass(TextOutputFormat.class);
		wcJob.waitForCompletion(true);
		System.out.println(wcJob.toString());
	}
}

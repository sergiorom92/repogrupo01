package uniandes.reuters.job;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import uniandes.reuters.util.XmlInputFormat;
import uniandes.reuters.mapRed.WCReducer;
import uniandes.reuters.mapRed.WCMapper;

import java.io.IOException;

public class WordCounterB {
	public static void main(String[] args)  {

		if(args.length<2){
			System.out.println("Se necesitan las carpetas de entrada y salida");
			System.exit(-1);
		}

		String entrada = args[0]; //carpeta de entrada
		String salida = args[1];//La carpeta de salida no puede existir


		try {
			ejecutarJob(entrada, salida);
		} catch (Exception e) { //Puede ser IOException, ClassNotFoundException o InterruptedException
			e.printStackTrace();
		}

	}
	public static void ejecutarJob(String entrada, String salida) throws IOException,ClassNotFoundException, InterruptedException
	{
		/**
		 * Objeto de configuraci�n, dependiendo de la versi�n de Hadoop
		 * uno u otro es requerido.
		 * */
		Configuration conf = new Configuration();
		conf.set("xmlinput.start", "<BODY>");
		conf.set("xmlinput.end", "</BODY>");
		Job wcJob=Job.getInstance(conf, "WordCounter Job B");
		wcJob.setJarByClass(WordCounterB.class);
		//////////////////////
		//Mapper
		//////////////////////

		wcJob.setMapperClass(WCMapper.class);

		wcJob.setMapOutputKeyClass(Text.class);
		wcJob.setMapOutputValueClass(IntWritable.class);
		///////////////////////////
		//Reducer
		///////////////////////////
		wcJob.setReducerClass(WCReducer.class);
		wcJob.setOutputKeyClass(Text.class);
		wcJob.setOutputValueClass(IntWritable.class);

		///////////////////////////
		//Input Format
		///////////////////////////
		//Advertencia: Hay dos clases con el mismo nombre,
		//pero no son equivalentes.
		//Se usa, en este caso, org.apache.hadoop.mapreduce.lib.input.TextInputFormat
		XmlInputFormat.setInputPaths(wcJob, new Path(entrada));
		wcJob.setInputFormatClass(XmlInputFormat.class);

		////////////////////
		///Output Format
		//////////////////////
		TextOutputFormat.setOutputPath(wcJob, new Path(salida));
		wcJob.setOutputFormatClass(TextOutputFormat.class);
		wcJob.waitForCompletion(true);
		System.out.println(wcJob.toString());
	}
}

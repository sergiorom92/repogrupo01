package uniandes.reuters.mapRed;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class WCReducerC3 extends Reducer<Text, IntWritable, Text, IntWritable> {
	int max_sum=0;
	int mean=0;
	int count=0;
	Text max_occured_key=new Text();
	int min_sum=Integer.MAX_VALUE;
	Text min_occured_key=new Text();

	public void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
		int sum = 0;
		for (IntWritable value : values) {
			sum += value.get();
			count++;
		}
		if(sum < min_sum) {
			min_sum= sum;
			min_occured_key.set(key);
		}
		if(sum > max_sum) {
			max_sum = sum;
			max_occured_key.set(key);
		}
		mean=max_sum+min_sum/count;
	}

	@Override
	protected void cleanup(Context context) throws IOException, InterruptedException {
		context.write(max_occured_key, new IntWritable(max_sum));
	}
}

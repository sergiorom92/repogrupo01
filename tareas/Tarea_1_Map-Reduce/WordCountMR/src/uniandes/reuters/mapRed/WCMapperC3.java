package uniandes.reuters.mapRed;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;


public class WCMapperC3 extends Mapper<LongWritable, Text, Text, IntWritable> {


	@Override
	protected void map(LongWritable key, Text value,
					   Context context)
			throws IOException, InterruptedException {
		String document = value.toString().replace("<PLACES>","@").replace("</PLACES>","@").
				replace("<PEOPLE>","@").replace("</PEOPLE>","@").
				replace("<ORGS>","@").replace("</ORGS>","@").
				replace("<EXCHANGES>","@").replace("</EXCHANGES>","@").
				replace("<COMPANIES>","@").replace("</COMPANIES>","@").
				replace("<UNKNOWN>","@").replace("</UNKNOWN>","@").
				replace("<TITLE>","@").replace("</TITLE>","@").
				replace("<DATELINE>","@").replace("</DATELINE>","@").
				replace("<BODY>","@").replace("</BODY>","@");
		String listWord[] = document.split("@");
		if (listWord.length == 18){
			String place = listWord[1].replace("<D>","").replace("</D>"," ");
			String body = listWord[17];
			String[] words = body.split("([().,!?:;'\"-></]|\\s)+");
			context.write(new Text( place ), new IntWritable(words.length));
		}
	}
}
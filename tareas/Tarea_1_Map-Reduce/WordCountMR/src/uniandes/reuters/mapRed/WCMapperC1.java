package uniandes.reuters.mapRed;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import uniandes.stopwords.StopWordsService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class WCMapperC1 extends Mapper<LongWritable, Text, Text, IntWritable> {

	public static final String START_TAG_KEY = "xmlinput.start";
	public static final String END_TAG_KEY = "xmlinput.end";
	
	@Override
	protected void map(LongWritable key, Text value,
			Context context)
			throws IOException, InterruptedException {

		Configuration conf = context.getConfiguration();
		String startTag = conf.get(START_TAG_KEY);
		String endTag = conf.get(END_TAG_KEY);

		String document = value.toString().replace(startTag,"").replace(endTag,"");

		String[] palabras = document.split("([().,!?:;'\"-></]|\\s)+");
		List<String> palabrasCount= new ArrayList<>();
		for(String palabra:palabras){
			if(!StopWordsService.isEnglishStopWord(palabra)) {
				palabrasCount.add(palabra);
			}
		}

		context.write(new Text("KEY"), new IntWritable(palabrasCount.size()));

	}
}

package uniandes.reuters.mapRed;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import uniandes.stopwords.StopWordsService;


public class WCMapperC2 extends Mapper<LongWritable, Text, Text, IntWritable> {


	@Override
	protected void map(LongWritable key, Text value,
					   Context context)
			throws IOException, InterruptedException {
		String document = value.toString().replace("<TITLE>","@").
				replace("</TITLE>","@").replace("<DATELINE>","@").
				replace("</DATELINE>","@").replace("<BODY>","@").replace("</BODY>","@");
		String listWord[] = document.split("@");
		StringBuilder word = new StringBuilder();
		if (listWord.length == 6){
			String title = listWord[1].replaceAll("([().,!?:;'\"-></]|\\s)+","");
			String date = listWord[3];
			String body = listWord[5];
			String[] palabras = body.split("([().,!?:;'\"-></]|\\s)+");
			for(String palabra:palabras){
				if(!StopWordsService.isEnglishStopWord(palabra)){
					if(word.length() == 0) {
						word.append(palabra);
					}
					else {
						word = palabra.length() > word.length() ? new StringBuilder(palabra) : word;
					}
				}
			}
			context.write(new Text(title + "|" + date + "|" + word), new IntWritable(word.length()));
		}
	}
}

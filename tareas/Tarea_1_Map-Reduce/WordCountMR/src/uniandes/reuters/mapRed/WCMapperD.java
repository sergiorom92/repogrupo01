package uniandes.reuters.mapRed;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import uniandes.stopwords.StopWordsService;

import java.io.IOException;
import java.util.HashMap;


public class WCMapperD extends Mapper<LongWritable, Text, Text, IntWritable> {

	public static final String START_TAG_KEY = "xmlinput.start";
	public static final String END_TAG_KEY = "xmlinput.end";

	@Override
	protected void map(LongWritable key, Text value,
			Context context)
			throws IOException, InterruptedException {
		String document;
		try {

			Configuration conf = context.getConfiguration();
			String startTag = conf.get(START_TAG_KEY);
			String endTag = conf.get(END_TAG_KEY);
			System.out.println(value.toString());
			document = value.toString().replace(startTag, "").
					replace(endTag, "").replace("<D>", ",").replace("</D>", ",");

			HashMap<String, Integer> palabrasLinea = new HashMap<String, Integer>();
			String[] palabras = document.split("([().,!?:;'\"-></]|\\s)+");
			for (String palabra : palabras) {
				String lw = palabra.toLowerCase().trim();
				if (lw.equals("")) {
					continue;
				} //No queremos contar espacios
				//Si la palabra existe en el hashmap incrementa en 1 su valor,
				//en caso contrario la agrega y le asigna 1.

				if (!StopWordsService.isEnglishStopWord(lw)) {
					palabrasLinea.put(lw,
							palabrasLinea.containsKey(lw) ?
									(palabrasLinea.get(lw) + 1)
									: 1);
				}

			}
			for (String k : palabrasLinea.keySet()) {
				context.write(new Text(k), new IntWritable(palabrasLinea.get(k)));
			}
		} catch (Exception e){
			System.out.println(e.getStackTrace());
		}
		
	}
}

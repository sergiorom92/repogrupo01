package uniandes.reuters.mapRed;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class WCReducerC1 extends Reducer<Text, IntWritable, Text, IntWritable> {
	private int totalArchivos =  0;
	@Override
	protected void reduce(Text key, Iterable<IntWritable> values,
			Context context)
			throws IOException, InterruptedException {
		int tot=0;
		totalArchivos=+1;
		for (IntWritable iw : values) {
			tot += iw.get();
		}
		context.write(key, new IntWritable(tot/totalArchivos));
		
	}

}

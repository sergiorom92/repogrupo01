
import os
import pyspark
import pandas as pd

import geopandas as gpd
from shapely.geometry import Point, Polygon, shape
from shapely import wkb, wkt



from pyspark.sql import Window
import pyspark.sql.functions as F
from pyspark.sql.types import TimestampType, IntegerType, StringType

from pyspark.sql import SparkSession
import functools

from pyspark.sql import DataFrame

spark = SparkSession.builder.getOrCreate() 
sc=spark.sparkContext

from pyspark.sql.types import StringType, IntegerType, FloatType, DoubleType,DecimalType
from pyspark.sql.functions import from_unixtime, unix_timestamp, col, udf
import shapely.speedups
shapely.speedups.enable()


filesPath = "/datos/taxis/individual/"
geoJsonPath = "/tmp/grupo01/bigdata01"
outputPath = "/user/bigdata01"


files_2009 = spark.read.format("csv").option("header","true").load(filesPath + "yellow_tripdata_2009*")
df_2009 = files_2009.withColumnRenamed("vendor_name","vendor_id").withColumnRenamed("Trip_Pickup_DateTime","pickup_datetime").withColumnRenamed("Trip_Distance","trip_distance").withColumnRenamed("End_Lat","dropoff_latitude").withColumnRenamed("End_Lon","dropoff_longitude").withColumnRenamed("Start_Lat","pickup_latitude").withColumnRenamed("Start_Lon","pickup_longitude").withColumnRenamed("Total_Amt","total_amount").select(["vendor_id","pickup_datetime","dropoff_latitude","dropoff_longitude",         "pickup_latitude","pickup_longitude","trip_distance","total_amount"])


files_2010 = spark.read.format("csv").option("header","true").load(filesPath + "yellow_tripdata_2010*")
df_2010 = files_2010.select(["vendor_id","pickup_datetime","dropoff_latitude","dropoff_longitude","pickup_latitude","pickup_longitude","trip_distance","total_amount"])


files_2011 = spark.read.format("csv").option("header","true").load(filesPath + "yellow_tripdata_2011*")
df_2011 = files_2011.select(["vendor_id","pickup_datetime","dropoff_latitude","dropoff_longitude","pickup_latitude","pickup_longitude","trip_distance","total_amount"])


files_2012 = spark.read.format("csv").option("header","true").load(filesPath + "yellow_tripdata_2012*")
df_2012 = files_2012.select(["vendor_id","pickup_datetime","dropoff_latitude","dropoff_longitude","pickup_latitude","pickup_longitude","trip_distance","total_amount"])


files_2013 = spark.read.format("csv").option("header","true").load(filesPath + "yellow_tripdata_2013*")
df_2013 = files_2013.select(["vendor_id","pickup_datetime","dropoff_latitude","dropoff_longitude",         "pickup_latitude","pickup_longitude","trip_distance","total_amount"])


files_2014 = spark.read.format("csv").option("header","true").load(filesPath + "yellow_tripdata_2014*")
df_2014 = files_2014.withColumnRenamed("vendor_id","vendor_id").withColumnRenamed(" pickup_datetime","pickup_datetime").withColumnRenamed(" trip_distance","trip_distance").withColumnRenamed(" dropoff_latitude","dropoff_latitude").withColumnRenamed(" dropoff_longitude","dropoff_longitude").withColumnRenamed(" pickup_latitude","pickup_latitude").withColumnRenamed(" pickup_longitude","pickup_longitude").withColumnRenamed(" total_amount","total_amount").select(["vendor_id","pickup_datetime","dropoff_latitude","dropoff_longitude",         "pickup_latitude","pickup_longitude","trip_distance","total_amount"])


files_2015 = spark.read.format("csv").option("header","true").load(filesPath + "yellow_tripdata_2015*")
df_2015 = files_2015.withColumnRenamed("VendorID","vendor_id").withColumnRenamed("tpep_pickup_datetime","pickup_datetime").withColumnRenamed("tpep_dropoff_datetime","dropoff_datetime").select(["vendor_id","pickup_datetime","dropoff_latitude","dropoff_longitude",         "pickup_latitude","pickup_longitude","trip_distance","total_amount"])


dfs = [df_2009,df_2010,df_2011,df_2012,df_2013,df_2014,df_2015]
taxis_data = functools.reduce(DataFrame.union,dfs)



taxis_data = taxis_data.withColumn("dropoff_latitude",col("dropoff_latitude").cast(DoubleType())).withColumn("trip_distance",col("trip_distance").cast(DoubleType())).withColumn("dropoff_longitude",col("dropoff_longitude").cast(DoubleType())).withColumn("pickup_latitude",col("pickup_latitude").cast(DoubleType())).withColumn("pickup_longitude",col("pickup_longitude").cast(DoubleType())).withColumn("total_amount",col("total_amount").cast(DoubleType()))

taxis_data_column = taxis_data.withColumn('year', F.col('pickup_datetime')[0:4])
df_1 = taxis_data_column.withColumn('month', F.col('pickup_datetime')[6:2])

df_json = gpd.read_file(geoJsonPath + "/geoJson.json")

gdf  = gpd.GeoDataFrame(df_json, geometry='geometry')

sc.broadcast(gdf)
def find_borough(latitude, longitude): 
    type(latitude)
    mgdf = gdf.apply(lambda x: x['borough'] if x['geometry'].intersects(Point(longitude,latitude)) else None, axis=1)
    idx = mgdf.first_valid_index()
    first_valid_value = mgdf.loc[idx] if idx is not None else None
    return first_valid_value
find_borough_udf = udf(find_borough, StringType())


udf_find_place = F.udf(find_borough, StringType())

data_df=df_1.withColumn('Start_Place', udf_find_place(F.col('pickup_latitude').cast(DoubleType()),F.col('pickup_longitude').cast(DoubleType()))).withColumn('End_Place', udf_find_place(F.col('dropoff_latitude').cast(DoubleType()),F.col('dropoff_longitude').cast(DoubleType())))                      
data_df.registerTempTable("temp");




count_df = (data_df
 .select("Start_Place", "End_Place")
 .where(data_df.Start_Place.isNotNull())
 .where(data_df.End_Place.isNotNull())
 .groupBy("Start_Place", "End_Place")
 .count().select('Start_Place',"End_Place", F.col('count'))
 )



 

df_pandas_start_end.to_csv( outputPath + '/export_dataframe_punto_C.csv', index = False)



end_by_year_df = (data_df
 .select("year", "End_Place")
 .where(data_df.year.isNotNull())
 .where(data_df.End_Place.isNotNull())
 .groupBy("year","End_Place")
 .count().select('year',"End_Place", F.col('count')).sort(data_df.year.desc())
 )


w = Window.partitionBy('year')
result_list=end_by_year_df.withColumn('count_1', F.max('count').over(w))    .where(F.col('count') == F.col('count_1'))    .drop('count_1')




result_list.toPandas().to_csv(outputPath + '/export_dataframe_punto_D.csv', index = False, header=True)





year_trip_dictance = data_df.groupBy("year").sum("trip_distance")


year_trip_dictance.toPandas().to_csv(outputPath + '/export_dataframe_punto_E.csv', index = False, header=True)






end_by_year_month_df = (data_df
 .select("year","month", "End_Place")
 .where(data_df.year.isNotNull())
 .where(data_df.End_Place.isNotNull())
 .groupBy("year","month","End_Place")
 .count().select('year',"month","End_Place", F.col('count')).sort(data_df.year.desc())
 )




w = Window.partitionBy('year','month')
result_list_by_month=end_by_year_month_df.withColumn('count_1', F.max('count').over(w))    .where(F.col('count') == F.col('count_1'))    .drop('count_1')




result_list_by_month.toPandas().to_csv(outputPath + '/export_dataframe_punto_F.csv', index = False, header=True)



end_by_year_total_amount_df = (data_df
 .select('year',"End_Place","total_amount")
 .where(data_df.year.isNotNull())
 .where(data_df.End_Place.isNotNull()))




end_by_year_total_amount = end_by_year_total_amount_df.groupBy("year","End_Place").max("total_amount")




end_by_year_total_amount.toPandas().to_csv(outputPath + '/export_dataframe_punto_G.csv', index = False, header=True)


const fs = require('fs');
const readline = require('readline');
const filesFolder = './files/';


async function getFirstLine(pathToFile) {
    const readable = fs.createReadStream(pathToFile);
    const reader = readline.createInterface({ input: readable });
    const line = await new Promise((resolve) => {
        reader.on('line', (line) => {
            reader.close();
            resolve(line);
        });
    });
    readable.close();
    return line;
}

function getDirectoryFiles(path) {
    return fs.readdirSync(path);
}

async function main() {
    const files = getDirectoryFiles(filesFolder);
    const obj = {};
    const fileContents = await Promise.all(
        files.map(async (filename) => {
            const obj = {};
            obj[filename] = await getFirstLine(`${filesFolder}/${filename}`);
            return obj;
        })
    )
    fileContents.forEach(fileContent => {
        Object.assign(obj, fileContent)
    })
    console.log(obj);
};

main();

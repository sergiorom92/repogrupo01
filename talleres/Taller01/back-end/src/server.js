import express from 'express';
import { MongoClient } from 'mongodb';

const app = express();
const port = 3000;

const corsMiddleware = (_, res, next) => {
  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', '*');

  // Request methods you wish to allow
  res.setHeader(
    'Access-Control-Allow-Methods',
    'GET, POST, OPTIONS, PUT, PATCH, DELETE',
  );

  // Request headers you wish to allow
  res.setHeader(
    'Access-Control-Allow-Headers',
    'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, Authorization',
  );

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', 'true');

  // Pass to next layer of middleware
  next();
};

app.use(corsMiddleware);

const mongoUri =
  'mongodb://root:rootpassword@localhost:27017/test?authSource=admin';

const mongoClient = new MongoClient(mongoUri);

/**
 * @param {{dbName:string,collectionName:string,filter:Object}} params
 */
const listAllDocuments = async ({ dbName, collectionName, filter }, res) => {
  const db = mongoClient.db(dbName);
  const collection = db.collection(collectionName);
  const all = await collection.find(filter).toArray();
  console.log({ all });
  res.send(all);
};

app.get('/', async (req, res) => {
  try {
    await mongoClient.connect();

    const filter = JSON.parse(req.query.filter);

    await listAllDocuments(
      {
        dbName: 'test',
        collectionName: 'testCollection',
        filter,
        // filter: { "age": { "$lte": 25 } }
      },
      res,
    );
  } catch (e) {
    console.error(e);
    res.status(500).send('ERROR');
  } finally {
    await mongoClient.close();
  }
});

app.listen(port, () => {
  console.log(`Server running on port: ${port}`);
});

#!/usr/bin/env python
# coding: utf-8


import pandas as pd
import geopandas as gpd
import csv


gdf = gpd.read_file("/home/bigdata01/s_11au16.shp")

pathParquetFile  ="/user/bigdata01/vasselInfo83"
pathCargo = "/home/bigdata01/CodeVesselTypes.csv"

pathFiles1 = "/datos/ais/2020/AIS_2020_03_05.csv.gz"
pathFiles2 = "/datos/ais/2020/AIS_2020_03_01.csv.gz"
pathFiles3 = "/datos/ais/2020/AIS_2020_12_19.csv.gz"
pathFiles4 = "/datos/ais/2020/AIS_2020_08_01.csv.gz"
pathFiles6 = "/datos/ais/2020/AIS_2020_11_30.csv.gz"
pathFiles7 = "/datos/ais/2020/AIS_2020_12_17.csv.gz"
pathFiles8 = "/datos/ais/2019/AIS_2019_03_24.csv.gz"
pathFiles9 = "/datos/ais/2019/AIS_2019_03_26.csv.gz"
pathFiles11 = "/datos/ais/2019/AIS_2019_11_16.csv.gz"
pathFiles12 = "/datos/ais/2019/AIS_2019_12_22.csv.gz"
pathFiles13 = "/datos/ais/2019/AIS_2019_12_19.csv.gz"
pathFiles14 = "/datos/ais/2019/AIS_2019_10_17.csv.gz"
pathFiles16 = "/datos/ais/2018/AIS_2018_01_14.csv.gz"
pathFiles17 = "/datos/ais/2018/AIS_2018_01_15.csv.gz"
pathFiles18 = "/datos/ais/2018/AIS_2018_03_24.csv.gz"
pathFiles19 = "/datos/ais/2018/AIS_2018_04_05.csv.gz"
pathFiles21 = "/datos/ais/2018/AIS_2018_03_23.csv.gz"
pathFiles23 = "/datos/ais/2017/AIS_2017_03_Zone07.csv.gz"
pathFiles24 = "/datos/ais/2017/AIS_2017_11_Zone07.csv.gz"
pathFiles26 = "/datos/ais/2017/AIS_2017_01_Zone07.csv.gz"
pathFiles27 = "/datos/ais/2017/AIS_2017_02_Zone07.csv.gz"
pathFiles28 = "/datos/ais/2017/AIS_2017_01_Zone01.csv.gz"


import pyspark
from pyspark.sql import SparkSession
import pyspark.sql.functions as F
from pyspark.sql.types import StringType, IntegerType, FloatType, DoubleType,DecimalType, StructType,StructField, TimestampType 
from shapely.geometry import Point, Polygon, shape
from shapely import wkb, wkt
import functools
from pyspark.sql import DataFrame

sc = SparkSession.builder.appName('Taller').\
config("spark.mongodb.input.uri", "mongodb://bigdata-mongodb-01.virtual.uniandes.edu.co:8087/bigdata01.vessels?retryWrites=false").\
config("spark.mongodb.output.uri", "mongodb://bigdata-mongodb-01.virtual.uniandes.edu.co:8087/bigdata01.vessels?retryWrites=false").\
config("spark.sql.execution.arrow.maxRecordsPerBatch","1000000").\
getOrCreate()
spark_context=sc.sparkContext


schema = StructType([
     StructField('MMSI', StringType(), True),
     StructField('BaseDateTime', TimestampType(), True),
     StructField('LAT', DoubleType(), True),
     StructField('LON', DoubleType(), True),
     StructField('SOG', DoubleType(), True),
     StructField('COG', DoubleType(), True),
     StructField('Heading', DoubleType(), True),
     StructField('VesselName', StringType(), True),
     StructField('IMO', StringType(), True),
     StructField('CallSign', StringType(), True),
     StructField('VesselType', StringType(), True),
     StructField('Status', StringType(), True),
     StructField('Length', StringType(), True),
     StructField('Width', StringType(), True),
     StructField('Draft', StringType(), True),
     StructField('Cargo', StringType(), True)
 ])


df1 = sc.read.schema(schema).csv(pathFiles1, header=True).select(['MMSI','BaseDateTime','LAT','LON','VesselType','Cargo'])


df = df1.filter("LAT IS NOT NULL AND LON IS NOT NULL AND Cargo IS NOT NULL AND VesselType IS NOT NULL").select(['MMSI','BaseDateTime','LAT','LON','VesselType','Cargo'])
df = df.withColumn("BaseDate",F.to_date("BaseDateTime"))


df =df.select(F.concat(dfData.MMSI,dfData.BaseDate).alias("Key"),'BaseDateTime','LAT','LON','VesselType','Cargo')
df =df.select(F.concat(df.MMSI,df.BaseDate).alias("Key"),'BaseDate','LAT','LON','VesselType','Cargo')

w = Window.partitionBy('Key')
df= df.withColumn('maxBaseDateTime', F.max('BaseDate').over(w),)\
    .where(F.col('BaseDate') == F.col('maxBaseDateTime'))\
    .drop('maxBaseDateTime')\
    .distinct()

def find_state(latitude, longitude): 
    points = gpd.GeoDataFrame(latitude,geometry=gpd.points_from_xy(longitude,latitude))
    points.crs={'init':'epsg:4269'}
    points = gpd.sjoin(points,gdf,how="left")
    return points['NAME'].head(len(latitude))

udf_find_state = F.pandas_udf(find_state, StringType())

dfData = df.select('Key','BaseDate','VesselType','Cargo', udf_find_state(F.col("LAT").cast(DoubleType()), F.col("LON").cast(DoubleType())).alias("State"))

dfData = dfData.filter('State IS NOT NULL')

dfData = dfData.withColumn("BaseYearMonth",F.substring(dfData.BaseDate,1,7))

dfData=dfData.select('Key','BaseDate','BaseYearMonth','Cargo','State','VesselType').distinct()

dictCodesCargoName = {}
dictCodesVesselTypeName = {}

with open(pathCargo, mode='r') as inp:
    reader = csv.reader(inp)
    dictCodesCargoName = {rows[0]:rows[2] for rows in reader}
    
with open(pathCargo, mode='r') as inp:
    reader = csv.reader(inp)
    dictCodesVesselTypeName = {rows[0]:rows[1] for rows in reader}


def find_state(latitude, longitude): 
    points = gpd.GeoDataFrame(latitude,geometry=gpd.points_from_xy(longitude,latitude))
    points.crs={'init':'epsg:4269'}
    points = gpd.sjoin(points,gdf,how="left")
    return points['NAME'].head(len(latitude))

dfData= dfData.replace(dictCodesCargoName,1,'Cargo')
dfData= dfData.replace(dictCodesVesselTypeName,1,'VesselType')

dfData = dfData.filter('Cargo IS NOT NULL AND VesselType IS NOT NULL')

dfData.write.parquet(pathParquetFile)
dfData.write.format("mongo").mode("append").save()                                                                    

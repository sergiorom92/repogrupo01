import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { HttpClient, HttpParams } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class DataService {
  constructor(private httpClient: HttpClient) {}
  fetchDocuments(data: string): BehaviorSubject<any> {
    const res = new BehaviorSubject([]);
    console.log({ data });
    let params = new HttpParams();

    //{"name": "haloo"}

    params = params.append("filter", data);
    console.log({ params });
    this.httpClient
      .get(`http://localhost:3000`, { params })
      .subscribe((data: any) => res.next(data));
    return res;
  }

  // fetchDocuments(data: string): BehaviorSubject<any> {
  //   const res = new BehaviorSubject([]);
  //   console.log({ data });
  //   let params = new HttpParams();
  //   params = params.append("filter", data);
  //   // params = params.append('var2', val2);

  //   // this.http.get(StaticSettings.BASE_URL, {params: params}).subscribe(...);
  //   // const params = new HttpParams();

  //   //{"name": "haloo"}

  //   // params.append("filter", data);
  //   console.log({ params });
  //   this.httpClient
  //     .get(`http://localhost:3000`, { params })
  //     .subscribe((data: any) => res.next(data));
  //   return res;
  // }

  sendHttpRequest(path: string): BehaviorSubject<any> {
    const result = new BehaviorSubject([]);
    this.httpClient.get(`http://172.24.99.146:8080/${path}`).subscribe(
      (data: any) => result.next(data),
      (error) => {
        console.log("oops", error);
        result.next([]);
      }
    );
    return result;
  }

  getPunto1Data$(): BehaviorSubject<any> {
    return this.sendHttpRequest(
      "traffic-state-period?start-date=2018-01-01&end-date=2018-12-31"
    );
  }

  getPunto2Data$(): BehaviorSubject<any> {
    return this.sendHttpRequest(
      "traffic-state-cargo-period?start-date=2014-01-21&end-date=2014-01-23"
    );
  }

  getPunto3Data$(): BehaviorSubject<any> {
    return this.sendHttpRequest("traffic-state-covid-period");
  }

  getPunto4Data$(): BehaviorSubject<any> {
    return this.sendHttpRequest(
      "traffic-vessel-period?start-date=2014-01-21&end-date=2014-01-23"
    );
  }

  getPunto5Data$(): BehaviorSubject<any> {
    return this.sendHttpRequest("traffic-vessel-relation-day");
  }

  getPuntoPregunta4Data$(): BehaviorSubject<any> {
    return this.sendHttpRequest("traffic-vessel-relation-fishing");
  }
}

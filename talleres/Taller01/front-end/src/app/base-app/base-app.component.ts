import { Component, OnInit } from "@angular/core";
import { BehaviorSubject, Subject } from "rxjs";
import { DataService } from "../data.service";

@Component({
  selector: "app-base-app",
  templateUrl: "./base-app.component.html",
  styleUrls: ["./base-app.component.scss"],
})
export class BaseAppComponent implements OnInit {
  punto1$: Subject<any[]> = new Subject();
  data: string = "";

  constructor(private dataService: DataService) {}

  ngOnInit(): void {
    this.punto1$.subscribe(data=>console.log(data));
    this.dataService
      .getPunto1Data$()
      .subscribe((data) => this.punto1$.next(data));

  }

}

import { ChangeDetectionStrategy, Component, OnInit } from "@angular/core";
import { BehaviorSubject, Subject } from "rxjs";
import { DataService } from "../data.service";

@Component({
  selector: "app-taller-dos",
  templateUrl: "./taller-dos.component.html",
  styleUrls: ["./taller-dos.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TallerDosComponent implements OnInit {
  documents$: BehaviorSubject<any> = new BehaviorSubject([]);
  // punto2$: BehaviorSubject<any> = new BehaviorSubject([]);
  // punto3$: BehaviorSubject<any> = new BehaviorSubject([]);
  // punto4$: BehaviorSubject<any> = new BehaviorSubject([]);
  // punto5$: BehaviorSubject<any> = new BehaviorSubject([]);
  // puntoPregunta4$: BehaviorSubject<any> = new BehaviorSubject([]);

  records = [];
  data: string = "";

  constructor(private dataService: DataService) {}

  fetchData() {
    this.documents$ = this.dataService.fetchDocuments(this.data);
  }

  ngOnInit(): void {
    // this.dataService
    //   .getPunto1Data$()
    //   .subscribe((data) => this.punto1$.next(data));
    //   this.dataService
    //     .getPunto2Data$()
    //     .subscribe((data) => this.punto2$.next(data));
    //   this.dataService
    //     .getPunto3Data$()
    //     .subscribe((data) => this.punto3$.next(data));
    //   this.dataService
    //     .getPunto4Data$()
    //     .subscribe((data) => this.punto4$.next(data));
    //   this.dataService
    //     .getPunto5Data$()
    //     .subscribe((data) => this.punto5$.next(data));
    //   this.dataService
    //     .getPuntoPregunta4Data$()
    //     .subscribe((data) => this.puntoPregunta4$.next(data));
  }
}
